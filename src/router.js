import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import SignIn from './views/SignIn.vue'
import SignUp from './views/SignUp.vue'
import WorkerCalendar from './views/WorkerCalendar.vue'
import ServiceAsCompany from './views/ServiceAsCompany.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/daxil-ol',
            name: 'sing-in',
            component: SignIn
        },
        {
            path: '/qeydiyyat',
            name: 'sing-up',
            component: SignUp
        },
        {
            path: '/workercalendar',
            name: 'workercalendar',
            component: WorkerCalendar
        },
        {
            path: '/serviceascompany',
            name: 'serviceascompany',
            component: ServiceAsCompany
        },
    ]
})
